package com.rocketmq.mq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootMqRocketmqApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootMqRocketmqApplication.class, args);
    }

}
