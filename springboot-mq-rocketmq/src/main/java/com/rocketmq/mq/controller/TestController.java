package com.rocketmq.mq.controller;

import com.rocketmq.mq.producer.Producer;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.remoting.exception.RemotingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.io.UnsupportedEncodingException;

/****************************************
 * 作者：HuMaSiYuan
 * 邮箱：siyueguoji@163.com
 * 日期：2023/10/11 9:07
 * 功能描述：
 ****************************************/
@Tag(name = "RocketMQ")
@RestController
public class TestController {

    @Autowired
    private Producer producer;

    @GetMapping(value = "/push")
    public String pushMsg(String msg) {
        try {
            return this.producer.send("PushTopic", "push", msg);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (MQBrokerException e) {
            e.printStackTrace();
        } catch (RemotingException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (MQClientException e) {
            e.printStackTrace();
        }
        return "ERROR";
    }

}
