package com.active.mq.config;

import jakarta.jms.Queue;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/****************************************
 * 作者：HuMaSiYuan
 * 邮箱：siyueguoji@163.com
 * 日期：2023/10/11 13:14
 * 功能描述：
 ****************************************/
@Configuration
public class QueueConfig {

    @Value(value = "${activemq.queueName}")
    private String queueName;

    @Bean(name = "queueName")
    public Queue queueName() {
        return new ActiveMQQueue(queueName);
    }
}
