package com.active.mq.producer;

import jakarta.jms.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsMessagingTemplate;
import org.springframework.stereotype.Component;

/****************************************
 * 作者：HuMaSiYuan
 * 邮箱：siyueguoji@163.com
 * 日期：2023/10/11 13:19
 * 功能描述：
 ****************************************/
@Component
public class Producer {

    @Autowired
    private JmsMessagingTemplate jmsMessagingTemplate;

    @Autowired
    private Queue queueName;


    public void sendMsg(String msg){
        this.jmsMessagingTemplate.convertAndSend(queueName, msg);
    }
}
