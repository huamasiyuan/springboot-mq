package com.active.mq.controller;

import com.active.mq.producer.Producer;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/****************************************
 * 作者：HuMaSiYuan
 * 邮箱：siyueguoji@163.com
 * 日期：2023/10/11 9:39
 * 功能描述：
 ****************************************/
@Tag(name = "ActiveMQ")
@RestController
public class TestController {

    @Autowired
    private Producer producer;

    @Operation(summary = "发送消息")
    @PostMapping(value="/sendMsg")
    public String sendMsg(String msg) {
        this.producer.sendMsg(msg);
        return "ok";
    }
}
