package com.active.mq.consumer;

import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

/****************************************
 * 作者：HuMaSiYuan
 * 邮箱：siyueguoji@163.com
 * 日期：2023/10/11 13:18
 * 功能描述：
 ****************************************/
@Component
public class Consumer {


    @JmsListener(destination = "testQueue")
    public void onMessage(String msg) {
        System.out.println("1111111111111");
        System.out.println("消费了消息：" + msg);
    }

}
