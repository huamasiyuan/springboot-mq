package com.mq.kafaka.controller;

import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.annotation.Resource;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/****************************************
 * 作者：HuMaSiYuan
 * 邮箱：siyueguoji@163.com
 * 日期：2023/10/8 14:12
 * 功能描述：
 ****************************************/
@Tag(name="生产者")
@RestController
@RequestMapping(value = "/producer")
public class ProducerController {

    @Resource
    private KafkaTemplate<String,String> kafka;

    @Operation(summary = "列表")
    @PostMapping("data")
    public String data(@RequestBody String msg) {
        kafka.send("test", msg);
        return "ok";
    }




}
