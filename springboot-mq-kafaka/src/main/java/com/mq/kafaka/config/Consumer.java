package com.mq.kafaka.config;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

/****************************************
 * 作者：HuMaSiYuan
 * 邮箱：siyueguoji@163.com
 * 日期：2023/10/8 16:11
 * 功能描述：
 ****************************************/
@Component
public class Consumer {


    @KafkaListener(topics = {"test"}, groupId = "kafka-test")
    public void onMessage(ConsumerRecord<String,String> record) {
        // 消费的哪个topic、partition的消息,打印出消息内容
        System.out.println("简单消费："+record.topic()+"-"+record.partition()+"-"+record.value());
    }



}
