package com.rabbit.mq.controller;

import com.rabbit.mq.producer.ApiCoreProducer;
import com.rabbit.mq.producer.ApiPaymentProducer;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/****************************************
 * 作者：HuMaSiYuan
 * 邮箱：siyueguoji@163.com
 * 日期：2023/10/12 10:33
 * 功能描述：TopicExchange是按规则转发消息，是交换机中最灵活的一个。也是最常用的一个
 *
 * 为什么api.core.user.query发送的消息没有被api.core队列监听消费？
 * 答：因为在TopicConfig配置类中，我们对api.core队列绑定的交换机规则是api.core.*，而通配符“*”只能向后多匹配一层路径。
 *
 * 为什么api.payment.order队列能监听并消费所有消息？
 * 答：*号只能向后多匹配一层路径。#号可以向后匹配多层路径。
 *
 ****************************************/
@Tag(name = "TopicExchange")
@RestController
public class TopicExchangeController {

    @Autowired
    private ApiCoreProducer apiCoreProducer;
    @Autowired
    private ApiPaymentProducer apiPaymentProducer;

    @Operation(summary = "TopicExchangeTest")
    @PostMapping(value = "/topicExchangeTest")
    public void start(String msg) {
        this.apiCoreSend(msg);
        this.apiCoreSendQuery(msg);
        this.apiPaymentOrder(msg);
        this.apiPaymentOrderQuery(msg);
        this.apiPaymentOrderDetailQuery(msg);
    }


    @PostMapping(value = "/apiCoreSend")
    public void apiCoreSend(String msg) {
        this.apiCoreProducer.send(msg);
    }



    @PostMapping(value = "/apiCoreSendQuery")
    public void apiCoreSendQuery(String msg) {
        this.apiCoreProducer.sendQuery(msg);
    }


    @PostMapping(value = "/apiPaymentOrder")
    public void apiPaymentOrder(String orderMsg) {
        this.apiPaymentProducer.order(orderMsg);
    }


    @PostMapping(value = "/apiPaymentOrderQuery")
    public void apiPaymentOrderQuery(String orderMsg) {
        this.apiPaymentProducer.orderQuery(orderMsg);
    }


    @PostMapping(value = "/apiPaymentOrderDetailQuery")
    public void apiPaymentOrderDetailQuery(String orderMsg) {
        this.apiPaymentProducer.orderDetailQuery(orderMsg);
    }
}
