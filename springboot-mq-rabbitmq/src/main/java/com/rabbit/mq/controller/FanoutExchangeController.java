package com.rabbit.mq.controller;

import com.rabbit.mq.producer.ApiReportProducer;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/****************************************
 * 作者：HuMaSiYuan
 * 邮箱：siyueguoji@163.com
 * 日期：2023/10/12 11:53
 * 功能描述：
 ****************************************/
@Tag(name = "FanoutExchange")
@RestController
public class FanoutExchangeController {

    @Autowired
    private ApiReportProducer producer;

    @PostMapping(value = "/fanoutExchangeTest")
    public void fanoutExchangeTest() {
        this.producer.generateReports("开始生成报表");
    }


}
