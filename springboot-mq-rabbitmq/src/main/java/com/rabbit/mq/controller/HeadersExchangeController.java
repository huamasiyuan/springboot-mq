package com.rabbit.mq.controller;

import com.rabbit.mq.producer.ApiCreditProducer;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/****************************************
 * 作者：HuMaSiYuan
 * 邮箱：siyueguoji@163.com
 * 日期：2023/10/12 11:20
 * 功能描述：
 ****************************************/
@Tag(name = "HeadersExchange")
@RestController
public class HeadersExchangeController {

    @Autowired
    private ApiCreditProducer producer;


    @Operation(summary = "HeadersExchangeTest")
    @PostMapping(value = "/headersExchangeTest")
    public void start() {
        this.sendCreditBankOfType();
        this.sendCreditBankOfAll();
        this.sendCreditFinanceOfType();
        this.sendCreditFinanceOfAll();
    }

    @PostMapping(value = "/bankOfType")
    public void sendCreditBankOfType() {
        Map<String,Object> head = new HashMap<>();
        head.put("type", "cash");
        this.producer.creditBank(head,"银行授信(部分匹配)");
    }

    @PostMapping(value = "/bankOfAll")
    public void sendCreditBankOfAll() {
        Map<String,Object> head = new HashMap<>();
        head.put("type", "cash");
        head.put("aging", "fast");
        this.producer.creditBank(head,"银行授信(全部匹配)");
    }

    @PostMapping(value = "/financeOfType")
    public void sendCreditFinanceOfType() {
        Map<String,Object> head = new HashMap<>();
        head.put("type", "cash");
        this.producer.creditFinance(head,"金融公司授信(部分匹配)");
    }

    @PostMapping(value = "/financeOfAll")
    public void sendCreditFinanceOfAll() {
        Map<String,Object> head = new HashMap<>();
        head.put("type", "cash");
        head.put("aging", "fast");
        this.producer.creditFinance(head,"金融公司授信(全部匹配)");
    }












}
