package com.rabbit.mq.consumer;

import com.rabbit.mq.po.Order;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;

/****************************************
 * 作者：HuMaSiYuan
 * 邮箱：siyueguoji@163.com
 * 日期：2023/10/12 13:49
 * 功能描述：
 ****************************************/
@Component
@RabbitListener(queues = "notify.refund")
public class RefundNotifyConsumer {


    @RabbitHandler
    public void onMessage(Order order) {
        System.out.println(MessageFormat.format("notify.refund receive message: [{0}]", order));
    }


}
