package com.rabbit.mq.consumer;

import com.rabbit.mq.po.Order;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/****************************************
 * 作者：HuMaSiYuan
 * 邮箱：siyueguoji@163.com
 * 日期：2023/10/12 14:06
 * 功能描述：
 ****************************************/
@Component
@RabbitListener(queues = "query.order")
public class QueryOrderConsumer {

    @RabbitHandler
    public Order receive(String orderId) {
        Order order = new Order();
        order.setOrderId(orderId);
        order.setGoodsName("华为云服务器");
        order.setGoodsNum("1");
        return order;
    }
}
