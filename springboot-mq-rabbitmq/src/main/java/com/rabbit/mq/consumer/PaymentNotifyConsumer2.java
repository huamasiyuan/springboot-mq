package com.rabbit.mq.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;

/****************************************
 * 作者：HuMaSiYuan
 * 邮箱：siyueguoji@163.com
 * 日期：2023/10/12 8:54
 * 功能描述：
 ****************************************/
@Component
@RabbitListener(queues = "notify.payment2")
public class PaymentNotifyConsumer2 {

    @RabbitHandler
    public void onMessage(String msg) throws InterruptedException {
        System.out.println(MessageFormat.format("notify.payment2消费者消费了消息[{0}]",msg));
    }

}
