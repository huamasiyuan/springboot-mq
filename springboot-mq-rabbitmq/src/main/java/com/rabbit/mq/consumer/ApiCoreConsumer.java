package com.rabbit.mq.consumer;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;

/****************************************
 * 作者：HuMaSiYuan
 * 邮箱：siyueguoji@163.com
 * 日期：2023/10/12 9:59
 * 功能描述：
 ****************************************/

@Component
public class ApiCoreConsumer {


    @RabbitHandler
    @RabbitListener(queues = "api.core")
    public void onMessage(String msg) {
        System.out.println(MessageFormat.format("核心队列消费者-消费了消息[{0}]",msg));
    }
}
