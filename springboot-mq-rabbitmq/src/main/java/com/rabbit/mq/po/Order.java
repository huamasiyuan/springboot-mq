package com.rabbit.mq.po;

import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/****************************************
 * 作者：HuMaSiYuan
 * 邮箱：siyueguoji@163.com
 * 日期：2023/10/12 13:51
 * 功能描述：注意，通过消息中间件发送的消息对象必须实现序列化
 ****************************************/
@Data
public class Order implements Serializable  {

    private static final long serialVersionUID = 8885519747277354525L;

    private String orderId;
    private String goodsName;
    private String goodsNum;

    @Override
    public String toString() {
        return "Order{" +
                "orderId='" + orderId + '\'' +
                ", goodsName='" + goodsName + '\'' +
                ", goodsNum='" + goodsNum + '\'' +
                '}';
    }
}
