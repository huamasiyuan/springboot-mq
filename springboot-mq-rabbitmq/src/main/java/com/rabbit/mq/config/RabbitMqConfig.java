package com.rabbit.mq.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/****************************************
 * 作者：HuMaSiYuan
 * 邮箱：siyueguoji@163.com
 * 日期：2023/10/11 16:01
 * 功能描述：
 ****************************************/
@Configuration
public class RabbitMqConfig {

    @Bean
    public Queue paymentNotifyQueue() {
        return new Queue("notify.payment");
    }

    @Bean
    public Queue paymentNotifyQueue2() {
        return new Queue("notify.payment2");
    }



}
