package com.rabbit.mq.config;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/****************************************
 * 作者：HuMaSiYuan
 * 邮箱：siyueguoji@163.com
 * 日期：2023/10/12 13:47
 * 功能描述： 发送整个对象
 ****************************************/

@Configuration
public class DirectConfig {

    @Bean
    public Queue refundNotNotifyQueue() {
        return new Queue("notify.refund");
    }

    // RPC队列
    @Bean
    public Queue queryOrderQueue() {
        return new Queue("query.order");
    }

}
