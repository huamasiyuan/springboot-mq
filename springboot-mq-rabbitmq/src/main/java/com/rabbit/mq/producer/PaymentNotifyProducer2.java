package com.rabbit.mq.producer;

import jakarta.annotation.Resource;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.stereotype.Component;

/****************************************
 * 作者：HuMaSiYuan
 * 邮箱：siyueguoji@163.com
 * 日期：2023/10/12 8:56
 * 功能描述：
 ****************************************/

@Component
public class PaymentNotifyProducer2 {

    @Resource
    private AmqpTemplate rabbitTemplate;

    public void sendMsg(String msg) {
        this.rabbitTemplate.convertAndSend("notify.payment2", msg);
    }


}
