package com.rabbit.mq.producer;

import com.rabbit.mq.po.Order;
import jakarta.annotation.Resource;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;

/****************************************
 * 作者：HuMaSiYuan
 * 邮箱：siyueguoji@163.com
 * 日期：2023/10/12 14:19
 * 功能描述：
 ****************************************/
@Component
public class QueryOrderProducer {

    @Resource
    private AmqpTemplate rabbitTemplate;


    public void sender(String orderId){
        // 通过RPC远程调用 返回订单结果
        Order order = (Order) rabbitTemplate.convertSendAndReceive("query.order", orderId);
        System.out.println(MessageFormat.format("query.order return message: [{0}]",order));
    }

}
