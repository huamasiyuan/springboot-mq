package com.rabbit.mq.producer;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;

/****************************************
 * 作者：HuMaSiYuan
 * 邮箱：siyueguoji@163.com
 * 日期：2023/10/12 10:03
 * 功能描述：
 ****************************************/
@Component
public class ApiCoreProducer {

    @Autowired
    private AmqpTemplate rabbitTemplate;


    public void send(String msg) {
        System.out.println(MessageFormat.format("api.core.send-发送了消息：[{0}]", msg));
        this.rabbitTemplate.convertAndSend("coreExchange", "api.core.send", msg);
    }

    public void sendQuery(String msg) {
        System.out.println(MessageFormat.format("api.core.send.query-发送了消息：[{0}]", msg));
        this.rabbitTemplate.convertAndSend("coreExchange", "api.core.send.query", msg);
    }

}
