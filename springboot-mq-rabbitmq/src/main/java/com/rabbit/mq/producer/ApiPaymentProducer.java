package com.rabbit.mq.producer;

import jakarta.annotation.Resource;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.stereotype.Component;
import java.text.MessageFormat;

/****************************************
 * 作者：HuMaSiYuan
 * 邮箱：siyueguoji@163.com
 * 日期：2023/10/12 10:09
 * 功能描述：
 ****************************************/
@Component
public class ApiPaymentProducer {

    @Resource
    private AmqpTemplate rabbitTemplate;

    public void order(String msg){
        System.out.println(MessageFormat.format("api.payment.order send message: [{0}]" ,msg));
        rabbitTemplate.convertAndSend("paymentExchange", "api.payment.order", msg);
    }

    public void orderQuery(String msg){
        System.out.println(MessageFormat.format("api.payment.order.query send message: [{0}]",msg));
        rabbitTemplate.convertAndSend("paymentExchange", "api.payment.order.query", msg);
    }

    public void orderDetailQuery(String msg){
        System.out.println(MessageFormat.format("api.payment.order.detail.query send message: [{0}]",msg));
        rabbitTemplate.convertAndSend("paymentExchange", "api.payment.order.detail.query", msg);
    }

}
