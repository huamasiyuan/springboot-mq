package com.rabbit.mq.producer;

import jakarta.annotation.Resource;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;

/****************************************
 * 作者：HuMaSiYuan
 * 邮箱：siyueguoji@163.com
 * 日期：2023/10/12 8:56
 * 功能描述：
 ****************************************/

@Component
public class PaymentNotifyProducer {

    @Resource
    private AmqpTemplate rabbitTemplate;

    public void sendMsg(String msg) {
        //System.out.println(MessageFormat.format("生产者发送消息:[{0}]",msg));
        this.rabbitTemplate.convertAndSend("notify.payment", msg);
    }


}
