package com.rabbit.mq.producer;

import jakarta.annotation.Resource;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;

/****************************************
 * 作者：HuMaSiYuan
 * 邮箱：siyueguoji@163.com
 * 日期：2023/10/12 11:51
 * 功能描述：
 ****************************************/
@Component
public class ApiReportProducer {

    @Resource
    private AmqpTemplate rabbitTemplate;

    public void generateReports(String msg){
        System.out.println(MessageFormat.format("api.generate.reports send message: [{0}]",msg));
        rabbitTemplate.convertAndSend("reportExchange", "api.generate.reports", msg);
    }
}
