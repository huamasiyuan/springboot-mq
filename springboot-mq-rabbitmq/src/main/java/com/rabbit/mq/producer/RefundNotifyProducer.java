package com.rabbit.mq.producer;

import com.rabbit.mq.po.Order;
import jakarta.annotation.Resource;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/****************************************
 * 作者：HuMaSiYuan
 * 邮箱：siyueguoji@163.com
 * 日期：2023/10/12 13:56
 * 功能描述：
 ****************************************/
@Component
public class RefundNotifyProducer {

    @Resource
    private AmqpTemplate rabbitTemplate;

    public void sender(Order order) {
        rabbitTemplate.convertAndSend("notify.refund", order);
    }
}
